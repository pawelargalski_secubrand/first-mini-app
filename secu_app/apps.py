from django.apps import AppConfig


class SecuAppConfig(AppConfig):
    name = 'secu_app'
